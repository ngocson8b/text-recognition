package test;

import java.io.File;
import java.net.URL;

import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.lept.PIX;
//import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacpp.tesseract.TessBaseAPI;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.OpenCVFrameConverter;

import static org.bytedeco.javacpp.opencv_core.IplImage;
//import org.bytedeco.javacpp.*;
import static org.bytedeco.javacpp.opencv_core.*;
//import static org.bytedeco.javacpp.opencv_highgui.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
//import org.bytedeco.javacpp.*; 
//import org.opencv.core.Core;
//import org.opencv.core.CvType;
//import org.opencv.core.Mat;
//import org.opencv.core.MatOfPoint;
//import org.opencv.core.Point;
//import org.opencv.core.Rect;
//import org.opencv.core.Scalar;
//import org.opencv.core.Size;
//import org.opencv.imgproc.Imgproc;
//import cl.eye.*;
//import org.opencv.*;
//import org.bytedeco.javacpp.opencv_objdetect;
//import org.bytedeco.javacpp.opencv_calib3d;
//import java.nio.file.*;
//import java.io.File;
//import org.bytedeco.javacv.CanvasFrame;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
//import org.bytedeco.javacpp.lept;
//
//import  org.bytedeco.javacpp.presets.tesseract;
//import java.awt.Image;    
//import java.io.File;    
//import java.io.FileInputStream;    
//import java.io.FileNotFoundException;    
//import java.io.IOException;    
//import java.io.InputStream;    
//import javax.imageio.ImageIO;   

//import org.bytedeco.javacpp.*;
import static org.bytedeco.javacpp.lept.*;
//import static org.bytedeco.javacpp.tesseract.*;


public class ReceiptScannerImpl {
	public static void main(String[] args) {
		ReceiptScannerImpll aaa = new ReceiptScannerImpll();
		aaa.getTextFromReceiptImage("test.jpg");
	}
}

class ReceiptScannerImpll implements ReceiptScanner {
	
	public interface ReceiptScanner {
		public String getTextFromReceiptImage(final String resceiptImageFilePath);
	}
	
	
	public String getTextFromReceiptImage(final String receiptFileImagePath) {
		// TODO Auto-generated method stub
		final File receiptImageFile = new File(receiptFileImagePath);
		final String receiptImagePathFile = receiptImageFile.getAbsolutePath();
		System.out.print('1'+ receiptImagePathFile);
		IplImage receiptImage = cvLoadImage(receiptImagePathFile);
		IplImage cannyEdgeImage = applyCannySquareEdgeDetectionOnImage(receiptImage, 30);
		
		CvSeq largestSquare = findLargestSquareOnCannyDetectedImage(cannyEdgeImage);
		
//		receiptImage = applyPerspectiveTransformThresholdOnOriginalImage(receiptImage, largestSquare, 30);
//		System.exit(0);
		
		receiptImage = cleanImageSmoothingForOCR(receiptImage);
//		System.exit(0);
		final File cleanedReceiptFile = new File(receiptImagePathFile);
		final String cleanedReceiptPathFile = cleanedReceiptFile.getAbsolutePath();
		cvSaveImage(cleanedReceiptPathFile, receiptImage);
		System.out.println(cleanedReceiptPathFile);
		cvReleaseImage(cannyEdgeImage);
		cannyEdgeImage = null;
		cvReleaseImage(receiptImage);
		receiptImage = null;
		return getStringFromImage(cleanedReceiptPathFile);
	}

	private String getStringFromImage(final String pathToReceiptImageFile) {
		try {
			final URL tessDataResource = getClass().getResource("/");
			final File tessFolder = new File(tessDataResource.toURI());
			final String tessFolderPath = tessFolder.getAbsolutePath();
			System.out.println(tessFolderPath);
			BytePointer outText;
			TessBaseAPI api = new TessBaseAPI();
			api.SetVariable("testedit_char_whitelist", "0123456789,/QWERTYUIOPASDFGHJKLZXCVBNM.,月、日、年、で、す、か");

			// Intialize tesseract-ocr with Spanish...
			if (api.Init(tessFolderPath, "jpn") != 0) {
				System.err.println("Could not tesseract. ");
			}

			// Open input image leptonica
			PIX image = pixRead(pathToReceiptImageFile);
			api.SetImage(image);
			// GEt OCR
			System.out.println("2xxxxxxxxxxxxxxxxx");


			outText = api.GetUTF8Text();
			System.out.println(outText);

			String string = outText.getString();
			System.out.println(string);
			api.End();
			outText.deallocate();
			pixDestroy(image);
			return string;
		} catch (Exception e) {
			System.out.println("1xxxxxxxxxxxxxx");
			e.printStackTrace();
			return null;
		}
	}

	private IplImage cleanImageSmoothingForOCR(IplImage srcImage) {
		IplImage destImage = cvCreateImage(cvGetSize(srcImage), IPL_DEPTH_8U, 1);
		cvCvtColor(srcImage, destImage, CV_BGR2GRAY);
		cvSmooth(destImage, destImage, CV_MEDIAN, 3, 0, 0, 0);
		cvThreshold(destImage, destImage, 0, 255, CV_THRESH_OTSU);
		return destImage;
	}

	private IplImage cropImage(IplImage srcImage, int fromX, int fromY, int toWidth, int toHeight) {
		cvSetImageROI(srcImage, cvRect(fromX, fromY, toWidth, toHeight));
		IplImage destImage = cvCloneImage(srcImage);
		cvCopy(srcImage, destImage);
		return destImage;
	}

	private IplImage applyPerspectiveTransformThresholdOnOriginalImage(IplImage srcImage, CvSeq contour, int percent) {
		IplImage warpImage = cvCloneImage(srcImage);

		for (int i = 0; i < contour.total(); i++) {
			CvPoint point = new CvPoint(cvGetSeqElem(contour, i));
			point.x((int) (point.x() * 100) / percent);
			point.y((int) (point.y() * 100) / percent);
		}

		CvPoint topRightPoint = new CvPoint(cvGetSeqElem(contour, 0));
		CvPoint topLeftPoint = new CvPoint(cvGetSeqElem(contour, 1));
		CvPoint bottomLeftPoint = new CvPoint(cvGetSeqElem(contour, 2));
		CvPoint bottomRightPoint = new CvPoint(cvGetSeqElem(contour, 3));

		int resultWidth = (int) (topRightPoint.x() - topLeftPoint.x());
		int bottomWidth = (int) (bottomRightPoint.x() - bottomLeftPoint.x());
		if (bottomWidth > resultWidth) {
			resultWidth = bottomWidth;
		}

		int resultHeight = (int) (bottomLeftPoint.y() - topLeftPoint.y());
		int bottomHeight = (int) (bottomRightPoint.y() - topRightPoint.y());
		if (bottomHeight > resultHeight) {
			resultHeight = bottomHeight;
		}

		float[] sourcePoints = { topLeftPoint.y(), topLeftPoint.y(), topRightPoint.x(), topRightPoint.y(),
				bottomLeftPoint.x(), bottomLeftPoint.y(), bottomRightPoint.x(), bottomRightPoint.y() };

		float[] destinationPoints = { 0, 0, resultWidth, 0, 0, resultHeight, resultWidth, resultHeight };
		CvMat homography = cvCreateMat(3, 3, CV_32FC1);
		cvGetPerspectiveTransform(sourcePoints, destinationPoints, homography);
		System.out.println(homography.toString());
		IplImage destImage = cvCloneImage(warpImage);
		cvWarpPerspective(warpImage, destImage, homography, CV_INTER_LINEAR, CvScalar.ZERO);
		return cropImage(destImage, 0, 0, resultWidth, resultHeight);
	}
	
	private CvSeq findLargestSquareOnCannyDetectedImage(IplImage cannyEdgeDetectedImage) {
		IplImage foundedContoursImage = cvCloneImage(cannyEdgeDetectedImage);
		CvMemStorage memory = CvMemStorage.create();
		CvSeq contours = new CvSeq();
		cvFindContours(foundedContoursImage, memory, contours, Loader.sizeof(CvContour.class), CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));
		int maxWidth = 0;
		int maxHeight = 0;
	
		CvRect contour = null;
		CvSeq seqFounded = null;
		CvSeq nextSeq = new CvSeq();
			for (nextSeq = contours; nextSeq != null; nextSeq = nextSeq.h_next()) {
				contour = cvBoundingRect(nextSeq, 0);
				if((contour.width() >= maxWidth) && (contour.height()>= maxHeight)) {
					maxWidth = contour.width();
					maxHeight = contour.height();
					seqFounded = nextSeq;
				}
			}
		
			CvSeq result = cvApproxPoly(seqFounded, Loader.sizeof(CvContour.class), memory, CV_POLY_APPROX_DP, cvContourPerimeter(seqFounded)*0.02, 0);
			for (int i=0; i<result.total(); i++) {
				CvPoint v= new CvPoint(cvGetSeqElem(result, i));
				cvDrawCircle(foundedContoursImage, v, 5, CvScalar.BLUE, 20, 8, 0);
				System.out.println("found point(" + v.x() + "," + v.y() + ")");
				
			}
			File f = new File(System.getProperty("user.home") + File.separator + "receipt-find-contours.jpeg");
			cvSaveImage(f.getAbsolutePath(), foundedContoursImage);
			return result;
	}
	
	private IplImage applyCannySquareEdgeDetectionOnImage(IplImage srcImage, int percent) {
		IplImage destImage = downScaleImage(srcImage, percent);
		IplImage grayImage = cvCreateImage(cvGetSize(destImage), IPL_DEPTH_8U, 1);
		
		// convert to gray
		
		cvCvtColor(destImage, grayImage, CV_BGR2GRAY);
		OpenCVFrameConverter.ToMat converterToMat = new OpenCVFrameConverter.ToMat();
		Frame grayImageFrame = converterToMat.convert(grayImage);
		Mat grayImageMat = converterToMat.convert(grayImageFrame);
		
		GaussianBlur(grayImageMat, grayImageMat, new Size(5, 5), 0.0, 0.0, BORDER_DEFAULT);
		 destImage = converterToMat.convertToIplImage(grayImageFrame);
		 
		 cvErode(destImage, destImage);
		 cvDilate(destImage, destImage);
		 
		 cvCanny(destImage, destImage, 75.0, 200.0);
		 File f = new File(System.getProperty("user.home") + File.separator + "receipt-canny-detect.jpeg");
		 
		cvSaveImage(f.getAbsolutePath(), destImage);
		return destImage;

	}
	
	private IplImage downScaleImage(IplImage srcImage, int percent) {
		System.out.println("srcImage- height - " + srcImage.height() + ", width - " + srcImage.width());
		
		IplImage destImage = cvCreateImage(
				cvSize((srcImage.width() * percent) / 100,
						(srcImage.height() * percent) / 100), srcImage.depth(),
						srcImage.nChannels());
		cvResize(srcImage,destImage);
		System.out.println("destImage - height - " + destImage.height() + ", width - " + destImage.width());
		return destImage;
	}

}
